CREATE DATABASE astro;

USE astro;

CREATE table photometry ( 
obj_id BIGINT, 
ra FLOAT, 
decl FLOAT, 
petroMag_r FLOAT, 
primary key (obj_id) );

LOAD DATA
INFILE "/tmp/Galaxies_photometry.csv"
INTO TABLE photometry
FIELDS TERMINATED BY ","
(obj_id, ra, decl, petroMag_r);

CREATE table spectroscopy ( 
bestobj_id BIGINT, 
ra FLOAT, 
decl FLOAT, 
z FLOAT, 
primary key (bestobj_id) );

LOAD DATA
INFILE "/tmp/Galaxies_spectroscopy.csv"
INTO TABLE spectroscopy
FIELDS TERMINATED BY ","
(bestobj_id, ra, decl, z);

CREATE table QSO_spectroscopy ( 
specobj_id BIGINT, 
specra FLOAT, 
specdecl FLOAT, 
specz FLOAT, 
primary key (specobj_id) );

LOAD DATA
INFILE "/tmp/QSOs_spectroscopy.csv"
INTO TABLE QSO_spectroscopy
FIELDS TERMINATED BY ","
(specobj_id, specra, specdecl, specz);

SELECT gp.obj_id, gs.bestobj_id, gp.petroMag_r, gs.z, qs.specz from photometry gp
JOIN spectroscopy gs on gp.obj_id = gs.bestobj_id
JOIN QSO_spectroscopy qs on gp.obj_id = qs.specobj_id
INTO OUTFILE '/Users/bmayura/Documents/data_obj_id.txt'; 

SELECT gp.obj_id, gp.ra, gp.decl, gp.petroMag_r, gs.z, qs.specz FROM photometry gp
JOIN spectroscopy gs ON (ABS(gs.ra-gp.ra) <= 0.0005) AND (ABS(gs.decl-gp.decl) <= 0.0005)
JOIN QSO_spectroscopy qs ON (ABS(qs.specra-gp.ra) <= 0.0005) AND (ABS(qs.specdecl-gp.decl) <= 0.0005)
INTO OUTFILE '/Users/bmayura/Documents/data_positions.txt'; 